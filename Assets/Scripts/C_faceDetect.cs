﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCVForUnity;

public class C_faceDetect : MonoBehaviour {

	VideoCapture m_VideoCapture = null;

	CascadeClassifier faceCascade;
	CascadeClassifier eyesCascade;

	private Texture2D myFace;
	public GameObject textureHolder;
	//List<Vector3> faceCollection = new List<Vector3>();  
	// Use this for initialization
	void Start () {

	m_VideoCapture = new VideoCapture(0);
		
	//instantiate cascade classifiers with relevant training data - MAKE SURE THE FILES EXIST.: 
	faceCascade=new CascadeClassifier(@"./Assets/OpenCVForUnity/StreamingAssets/haarcascade_frontalface_alt.xml");
	eyesCascade=new CascadeClassifier(@"./Assets/Scripts/haarcascade_eye.xml");


	}
	
	// Update is called once per frame
	void Update () {

		//staging variables:
		Mat frontGrab = new Mat();
		Mat gray = new Mat();
		Mat faceMat = null;

		Mat colouredOutput = null;
		
		MatOfRect faceCollection = new MatOfRect(); //contains ROIs of detected faces
		MatOfRect eyesCollection = new MatOfRect(); //contains ROIs of detected eyes

		m_VideoCapture.retrieve(frontGrab);
	
		Imgproc.cvtColor(frontGrab,gray,Imgproc.COLOR_BGR2GRAY); //map captured image to grayscale to enhance face-detection

		//do facedetection:
		faceCascade.detectMultiScale(gray,faceCollection,1.05,3,0 
		| Objdetect.CASCADE_FIND_BIGGEST_OBJECT 
		| Objdetect.CASCADE_SCALE_IMAGE
		,new Size(30,30),new Size());

		//print(faceCollection.size());
		//print("FaceCollection: "+ faceCollection);
	
	if(faceCollection.get(0,0)!=null) //if there's a face.
	{
		OpenCVForUnity.Rect face = faceCollection.toArray()[0]; //get ROI from first detected face

	   			//why the hell would Imgproc.rectangle not take a OpenCVForUnity.Rect?!
	  Imgproc.rectangle(gray,new Point(face.x,face.y),new Point(face.x+face.width,face.y+face.height),new Scalar(255,0,0),2); //Draw square around first detected face
		

	   faceMat = new Mat(gray,faceCollection.toArray()[0]); //do a new mat based on grayscale image and the ROI from facedetection 
	   faceMat = faceMat.clone(); //explicitly copy the data from 'gray' to avoid weird pointer stuff. (memory intensive)

		colouredOutput = new Mat(frontGrab,faceCollection.toArray()[0]);
		colouredOutput = colouredOutput.clone();	

	   eyesCascade.detectMultiScale(faceMat,eyesCollection,1.05,3,0 
		| Objdetect.CASCADE_DO_ROUGH_SEARCH 
		| Objdetect.CASCADE_SCALE_IMAGE
		,new Size(30,30),new Size()); //do eye-detection on this new mat



		//if two eyes are detected
		if(eyesCollection.size().Equals(new Size(1,2))){ 
		OpenCVForUnity.Rect e1 = eyesCollection.toArray()[0]; //get ROI
		Imgproc.circle(colouredOutput,new Point(e1.x+e1.width/2,e1.y+e1.height/2),(int)e1.width/2,new Scalar(0,0,255,0),4,Imgproc.LINE_AA,0); //DRAW CIRCLE
		
		OpenCVForUnity.Rect e2 = eyesCollection.toArray()[1]; //get ROI
		Imgproc.circle(colouredOutput,new Point(e2.x+e2.width/2,e2.y+e2.height/2),(int)e2.width/2,new Scalar(0,0,255,0),4,Imgproc.LINE_AA,0); //DRAW CIRCLE			
		}

		//if only one eye is detected
		else if(eyesCollection.size().Equals(new Size(1,1))){ 
		OpenCVForUnity.Rect e1 = eyesCollection.toArray()[0]; //get ROI
		Imgproc.circle(colouredOutput,new Point(e1.x+e1.width/2,e1.y+e1.height/2),(int)e1.width/2,new Scalar(0,0,255,0),4,Imgproc.LINE_AA,0); //DRAW CIRCLE
		}

		
	}
		//MatDisplay.DisplayMat(gray,MatDisplaySettings.FULL_BACKGROUND);		
		//if(faceMat != null) MatDisplay.DisplayMat(faceMat,MatDisplaySettings.BOTTOM_RIGHT);

		if(colouredOutput != null) {
		Imgproc.cvtColor(colouredOutput,colouredOutput,Imgproc.COLOR_BGR2RGB); //looks better.
		//MatDisplay.DisplayMat(colouredOutput,MatDisplaySettings.FULL_BACKGROUND);

		MatDisplay.MatToTexture(colouredOutput, ref myFace);
        textureHolder.GetComponent<Renderer>().material.mainTexture = myFace;

		//READY TO APPLY colouredOutput to surface of image target.
		
		}

			

	}
}
