﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterPicker : MonoBehaviour {

    GameObject letterPickerObject;
    TextMesh letterPicker;

    // Use this for initialization
    void Start () {
        letterPickerObject =  GameObject.Find("PostIt3/LetterPicker");
        letterPicker = letterPickerObject.GetComponent<TextMesh>();

        letterPicker.text = "MARK ER GRIM";

    }
	
	// Update is called once per frame
	void Update () {
    }
}
