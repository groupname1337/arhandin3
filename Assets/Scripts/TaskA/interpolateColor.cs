﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interpolateColor : MonoBehaviour {

	Color color;
	GameObject[] postIts;
	ActivePostIt[] activePostIts;
	public float grade = 1;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		postIts = GameObject.FindGameObjectsWithTag("Post-it");

		foreach (GameObject postIt in postIts) {
			if(AreNear(transform.position, postIt.transform.position)) {
				Renderer postItRend = postIt.GetComponent<Renderer>();
				postItRend.material.shader = Shader.Find("Standard");
				Color initialColor = postItRend.material.GetColor("_Color");
				color = Color.Lerp(initialColor, Color.black, grade);
				postItRend.material.SetColor("_Color", color);

				
			}
		}
	}

	  bool AreNear(Vector3 obj1, Vector3 obj2, float minDistance = 0.05f) {
         Vector3 between = obj1 - obj2;
		 return (between.magnitude < minDistance);
    }

private class ActivePostIt {
	public GameObject activePostIt {get; set;}
	public Quaternion initialRotation {get; set;}
	public ActivePostIt(GameObject activePostIt, Quaternion initialRotation){
		this.activePostIt = activePostIt;
		this.initialRotation = initialRotation;

	}
}
}
