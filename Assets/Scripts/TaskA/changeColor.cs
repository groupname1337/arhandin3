﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeColor : MonoBehaviour {

	Color color;
	GameObject[] postIts;
	List<ActivePostIt> activePostIts;

	List<GameObject> postItsCloseTo;
	// Use this for initialization
	void Start () {
		Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Standard");
		color = rend.material.GetColor("_Color");
		activePostIts = new List<ActivePostIt>();
	}
	
	// Update is called once per frame
	void Update () {
		postIts = GameObject.FindGameObjectsWithTag("Post-it");
	 	postItsCloseTo = new List<GameObject>();
		

		foreach (GameObject postIt in postIts) {
			if(AreNear(transform.position, postIt.transform.position)) {
				postItsCloseTo.Add(postIt);
				Renderer postItRend = postIt.GetComponent<Renderer>();
        		postItRend.material.SetColor("_Color", color);
			}
		}
		
		activePostIts.ForEach(XORFilter); //XOR active and nearby postits

		postItsCloseTo.ForEach(AddToactivePostIts); // Add new actives

		foreach(ActivePostIt postIt in activePostIts) {
			float diff = postIt.initialRotation.y - postIt.activePostIt.transform.rotation.y;
			Color lerpColor = Color.Lerp(color, Color.black, Mathf.Abs(diff));
			Renderer postItRend = postIt.activePostIt.GetComponent<Renderer>();
        	postItRend.material.SetColor("_Color", lerpColor);
		}
	}

	//This method detects if the object are close depending on minDistance value
     bool AreNear(Vector3 obj1, Vector3 obj2, float minDistance = 0.05f) {
         Vector3 between = obj1 - obj2;
		 return (between.magnitude < minDistance);
    }

	void XORFilter(ActivePostIt postItwrapper) {
		IndexDelegate Find = (pT) => postItsCloseTo.IndexOf(postItwrapper.activePostIt);
		int index = Find(postItwrapper);
		if(index == -1) {
			activePostIts.Remove(postItwrapper);
		} else {
			postItsCloseTo.RemoveAt(index);
		}
		
	}

	void AddToactivePostIts(GameObject newPostIt) {
		Quaternion rotation = newPostIt.transform.rotation;
		ActivePostIt activePostIt = new ActivePostIt(newPostIt, rotation);
		activePostIts.Add(activePostIt);
	}
	
private class ActivePostIt {
	public GameObject activePostIt {get; set;}
	public Quaternion initialRotation {get; set;}
	public ActivePostIt(GameObject activePostIt, Quaternion initialRotation){
		this.activePostIt = activePostIt;
		this.initialRotation = initialRotation;

	}
}

delegate int IndexDelegate(ActivePostIt postIt);
}
