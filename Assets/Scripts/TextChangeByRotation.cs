﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextChangeByRotation : MonoBehaviour {

    public GameObject rotationObject;

    private TextMesh letterToDisplay;
    private float yRotation;

    private float interval;

    private string[] letterArray = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "_"};
    private float[] ranges;

    // Use this for initialization
    void Start () {
        letterToDisplay = GetComponent<TextMesh>();

        interval = 360f / letterArray.Length;
        print(interval); //12

        ranges = new float[letterArray.Length];

        for (int i = 0; i < ranges.Length; i++)
        {
            ranges[i] = interval * i;
        }


        print("ranges: " + ranges[27]);
        print("ranges length: " + ranges.Length);
    }
	
	// Update is called once per frame
	void Update () {
        yRotation = rotationObject.transform.rotation.eulerAngles.y;

        int i = 0;
        foreach (float a in ranges)
        {
            if(a <= yRotation && yRotation < (a + interval))
            {
                letterToDisplay.text = letterArray[i];   
            }
            ++i;
        }

        //print(rotationObject.transform.rotation);
    }
}