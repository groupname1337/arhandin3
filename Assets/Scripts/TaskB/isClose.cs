﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class isClose : MonoBehaviour, IVirtualButtonEventHandler
{
	GameObject[] postIts;
	List<GameObject> activePostIts;

	List<GameObject> postItsCloseTo;


    private Ray ray;
    private Material material;

    // Use this for initialization
    void Start () {
        
        postItsCloseTo = new List<GameObject>();

        
        //Register eventhandlers for button
        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vbs.Length; ++i)
        {
            vbs[i].RegisterEventHandler(this);
        }
        

    }

    // Update is called once per frame
    void Update () {
		postIts = GameObject.FindGameObjectsWithTag("Post-it");
        
	}

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        print("Button Pressed " + this);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        print("Button Released " + this);
        //When button released, look through postIts
        foreach (GameObject postIt in postIts)
        {
            isClose postIt1 = postIt.GetComponent<isClose>();
            
            // if post it is near, add to CloseTo list
            if (AreNear(transform.transform.position, postIt.transform.position))
            {
                postItsCloseTo.Add(postIt);
                postIt1.Friend(gameObject);

            }
            //else remove from list.
            else if (postItsCloseTo.Contains(postIt))
            {
                postItsCloseTo.Remove(postIt);
                postIt1.Unfriend(gameObject);
            }

        }
    }
    //Check if two vectors are near
    bool AreNear(Vector3 obj1, Vector3 obj2, float minDistance = 0.15f) {

        Vector3 between = obj1 - obj2;
        float distance = between.magnitude;
		return (0.01f < distance && distance < minDistance);
    }

    //Remove linked post-it from list
    public void Unfriend(GameObject obj)
    {
        if (postItsCloseTo.Contains(obj))
        {
            print("(in IF) Unfriend " + obj);
            postItsCloseTo.Remove(obj);
        }
    }
    //Add linked post-it to list
    public void Friend(GameObject obj)
    {
        if (!postItsCloseTo.Contains(obj))
        {
            print("(in IF) friend " + obj);
            postItsCloseTo.Add(obj);
        }
    }
    //Method that draws lines.
    private void OnRenderObject()
    {
        
        if (material == null)
        {
            material = new Material(Shader.Find("Hidden/Internal-Colored"));
        }

        material.SetPass(0);
        ray.origin = transform.position;
        //Draw line for each object in CloseTo-list
        foreach (GameObject obj in postItsCloseTo)
        {
            ray.direction = (obj.transform.position - transform.position).normalized;

            GL.PushMatrix();
            GL.Begin(GL.LINES);
            GL.Color(Color.red);
            GL.Vertex(ray.origin);
            GL.Vertex(ray.origin + ray.direction * (obj.transform.position - transform.position).magnitude);
            GL.End();
            GL.PopMatrix();

        }
        
        //Debug.DrawRay(ray.origin, ray.direction * (obj2 - obj1).magnitude, Color.blue);

       
    }
}
