﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using OpenCVForUnity;

public class CornerTracking : MonoBehaviour {

    public GameObject corner1;
    public GameObject corner2;
    public GameObject corner3;
    public GameObject corner4;

    public GameObject drawP1;
    public GameObject drawP2;
    public GameObject drawP3;
    public GameObject drawP4;

    public bool debugging = false;
    public bool drawCircles = false;

    private float targetHeight;
    private float targetWidth;

    public Camera cam;
    private Mat camImageMat;

    MatOfPoint2f drawPDest = new MatOfPoint2f();
    MatOfPoint2f drawPSrc = new MatOfPoint2f();
    MatOfPoint2f drawTDest = new MatOfPoint2f();
    MatOfPoint2f drawTSrc = new MatOfPoint2f();

    private Scalar lowerHand = new Scalar(0, 48, 80);
    private Scalar upperHand = new Scalar(100, 255, 255);

    public float thresMin = 0.0f;
    public float thresMax = 255.0f;

    private Mat drawMat;

    // Use this for initialization
    void Start () {

        // caluclate dimensions for the destination 
        targetHeight = Mathf.Round(Mathf.Abs(corner1.transform.position.z - corner3.transform.position.z) * 1000f );
        targetWidth = Mathf.Round(Mathf.Abs(corner1.transform.position.x - corner2.transform.position.x)*1000f);
        drawMat = new Mat((int) targetHeight, (int) targetWidth, CvType.CV_8UC4);
        print("Target Dimensions: " + targetWidth + "x" + targetHeight);

        // memory allocation for point matrices
        drawPDest.alloc(4);
        drawPSrc.alloc(4);

        drawTDest.alloc(4);
        drawTSrc.alloc(4);

    }

    // Update is called once per frame
    void Update () {

        Image camImg = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.RGBA8888);
        if(camImg != null)
        {
            camImageMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
            camImageMat.put(0, 0, camImg.Pixels);

            Matrix4x4 A = Matrix4x4.identity;
            A.m00 = 650f;
            A.m11 = 650f;
            A.m02 = 320f;
            A.m12 = 240f;

            Matrix4x4 Rt = cam.transform.localToWorldMatrix;
            Matrix4x4 worldToImage = A * Rt;
          
            Vector3 duv1 = worldToImage.MultiplyPoint(drawP1.transform.position);
            Vector3 duv2 = worldToImage.MultiplyPoint(drawP2.transform.position);
            Vector3 duv3 = worldToImage.MultiplyPoint(drawP3.transform.position);
            Vector3 duv4 = worldToImage.MultiplyPoint(drawP4.transform.position);

            int height = camImageMat.height();
            Vector2 dnUV1 = new Vector2(duv1.x / duv1.z, height - duv1.y / duv1.z);
            Vector2 dnUV2 = new Vector2(duv2.x / duv2.z, height - duv2.y / duv2.z);
            Vector2 dnUV3 = new Vector2(duv3.x / duv3.z, height - duv3.y / duv3.z);
            Vector2 dnUV4 = new Vector2(duv4.x / duv4.z, height - duv4.y / duv4.z);

            if (drawCircles)
            {
                Imgproc.circle(camImageMat, new Point(dnUV1.x, dnUV1.y), 3, new Scalar(255, 0, 0, 255));
                Imgproc.circle(camImageMat, new Point(dnUV2.x, dnUV2.y), 3, new Scalar(255, 128, 0, 255));
                Imgproc.circle(camImageMat, new Point(dnUV3.x, dnUV3.y), 3, new Scalar(0, 255, 0, 255));
                Imgproc.circle(camImageMat, new Point(dnUV4.x, dnUV4.y), 3, new Scalar(0, 0, 255, 255));
            }

            // convert points
            drawPSrc.put(0, 0, dnUV1.x, dnUV1.y);
            drawPSrc.put(1, 0, dnUV2.x, dnUV2.y);
            drawPSrc.put(2, 0, dnUV3.x, dnUV3.y);
            drawPSrc.put(3, 0, dnUV4.x, dnUV4.y);

            drawPDest.put(0, 0, 0, 0);
            drawPDest.put(1, 0, 32, 0);
            drawPDest.put(2, 0, 0, 32);
            drawPDest.put(3, 0, 32, 32);

            // warp out the rect to a mat
            Mat drawHomograpy = Calib3d.findHomography(drawPSrc, drawPDest);
            Mat drawToggleMat = new Mat();
            Imgproc.warpPerspective(camImageMat, drawToggleMat, drawHomograpy, new Size(32, 32));
            
            Mat toggleMatHSV = new Mat();
            Imgproc.cvtColor(drawToggleMat, toggleMatHSV, Imgproc.COLOR_RGB2HSV_FULL);

            Mat ranged = new Mat();
            // toggle drawing
            Core.inRange(toggleMatHSV, lowerHand, upperHand, ranged);
            
            double currentRange = toggleMatHSV.get(16, 16)[0];
            
            //doubletoggleMatHSV.get(0, 0);
            /*
            List<double> h = new List<double>();
            List<double> s = new List<double>();
            List<double> v = new List<double>();

            for(int i = 0; i < toggleMatHSV.rows(); i++)
            {
                for (int j = 0; j < toggleMatHSV.cols(); j++)
                {
                    double[] pixel = toggleMatHSV.get(i, j);
                    Debug.Log("Pixel: " + pixel);
                    h.Add(pixel[0]);
                    s.Add(pixel[1]);
                    v.Add(pixel[2]);
                }
            }

            h.Sort(); s.Sort(); v.Sort();

            // lower bounds
            double lowerHandH = (h[0] + h[1] + h[2] + h[3]) / 4;
            double upperHandH = (h[h.Count-4] + h[h.Count-3] + h[h.Count-2] + h[h.Count-1]) / 4;

            double lowerHandS = (s[0] + s[1] + s[2] + s[3]) / 4;
            double upperHandS = (s[s.Count - 4] + s[s.Count - 3] + s[s.Count - 2] + s[s.Count - 1]) / 4;

            double lowerHandV = (v[0] + v[1] + v[2] + v[3]) / 4;
            double upperHandV = (v[v.Count - 4] + v[v.Count - 3] + v[v.Count - 2] + v[v.Count - 1]) / 4;*/
            /*
            lowerHand = new Scalar(lowerHandH, lowerHandS, lowerHandV);
            upperHand = new Scalar(upperHandH, upperHandS, upperHandV);*/
           

            Mat targetMat = new Mat();
            Mat skinInRange = new Mat();
            Mat contourMat = null;
            Mat warpedDrawMat = new Mat();
            print("Range: " + currentRange);
            if (currentRange < 25 && currentRange > 2)
            {
                // yay, we are supposed to draw stuff now! :) 
                // but lets start by tracking the hand - lets warp the inner part of the image target to a new mat
                Vector3 tuv1 = worldToImage.MultiplyPoint(corner1.transform.position);
                Vector3 tuv2 = worldToImage.MultiplyPoint(corner2.transform.position);
                Vector3 tuv3 = worldToImage.MultiplyPoint(corner3.transform.position);
                Vector3 tuv4 = worldToImage.MultiplyPoint(corner4.transform.position);

                Vector2 tnUV1 = new Vector2(tuv1.x / tuv1.z, height - tuv1.y / tuv1.z);
                Vector2 tnUV2 = new Vector2(tuv2.x / tuv2.z, height - tuv2.y / tuv2.z);
                Vector2 tnUV3 = new Vector2(tuv3.x / tuv3.z, height - tuv3.y / tuv3.z);
                Vector2 tnUV4 = new Vector2(tuv4.x / tuv4.z, height - tuv4.y / tuv4.z);

                // convert points
                drawTSrc.put(0, 0, tnUV1.x, tnUV1.y);
                drawTSrc.put(1, 0, tnUV2.x, tnUV2.y);
                drawTSrc.put(2, 0, tnUV3.x, tnUV3.y);
                drawTSrc.put(3, 0, tnUV4.x, tnUV4.y);

                drawTDest.put(0, 0, 0, 0);
                drawTDest.put(1, 0, targetWidth, 0);
                drawTDest.put(2, 0, 0, targetHeight);
                drawTDest.put(3, 0, targetWidth, targetHeight);


                Mat targetHomograpy = Calib3d.findHomography(drawTSrc, drawTDest);
                Imgproc.warpPerspective(camImageMat, targetMat, targetHomograpy, new Size(targetWidth, targetHeight));

                // cool, so now lets do the contour thing
                // create an inrange mat
                Mat targetHSV = new Mat();
                Imgproc.cvtColor(targetMat, targetHSV, Imgproc.COLOR_RGB2HSV_FULL);
                Core.inRange(targetHSV, lowerHand, upperHand, skinInRange);

                // pretty cool, lets contour it... cuz yeah!
                // find countours using opencv
                Mat bw = new Mat();

                //Imgproc.cvtColor(skinInRange, bw, Imgproc.COLOR_RGBA2GRAY);
                Mat threshold = new Mat(targetHSV.height(), targetHSV.width(), CvType.CV_8UC4);
                Imgproc.threshold(skinInRange, threshold, thresMin, thresMax, Imgproc.THRESH_BINARY);

                // find countours using opencv
                Mat contours = new Mat();
                List<MatOfPoint> points = new List<MatOfPoint>();
                Imgproc.findContours(threshold, points, contours, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
                Scalar color = new Scalar(255, 0, 0);
                contourMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
                Imgproc.drawContours(contourMat, points, -1, color, 5);

                Mat polygonMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
                List<MatOfPoint2f> aprox = new List<MatOfPoint2f>();
                foreach (MatOfPoint point in points)
                {
                    MatOfPoint2f p = new MatOfPoint2f();
                    MatOfPoint2f ap = new MatOfPoint2f();

                    point.convertTo(p, CvType.CV_32F);
                    Imgproc.approxPolyDP(p, ap, 10f, true);

                    aprox.Add(ap);
                }

                //print(aprox);
                Point pointToDraw = new Point(1000, 1000);
                foreach(MatOfPoint2f p in aprox)
                {
                    // we find the point with the 
                    for(int i = 0; i < p.rows(); i++)
                    {
                        for (int j = 0; j < p.cols(); j++)
                        {
                            double[] point = p.get(i, j);
                            //print("Point: " + point[0] + ", " + point[1]);
                            if (point[1] < pointToDraw.y);
                            {
                                pointToDraw = new Point(point[0], point[1]);
                            }

                        }
                    }
                }

                //print("Point to draw: " + pointToDraw[0] + ", "+pointToDraw[1]);
                Debug.Log("Point to draw "+ pointToDraw);
                Imgproc.circle(drawMat, pointToDraw, 10, new Scalar(255, 0, 0, 255), -1, 8, 0);

                // warp the draw mat back at at it
                Imgproc.warpPerspective(drawMat, warpedDrawMat, targetHomograpy.inv(), new Size(640, 480));

            }

            if (debugging) {
                
            // display the background video
            MatDisplay.DisplayMat(camImageMat, MatDisplaySettings.FULL_BACKGROUND);

            // display Draw toggle area
            MatDisplay.DisplayMat(drawToggleMat, MatDisplaySettings.BOTTOM_LEFT);
            // display inRangeThingForDebugging
            //MatDisplay.DisplayMat(toggleMatHSV, MatDisplaySettings.BOTTOM_RIGHT);

            //MatDisplay.DisplayMat(contourMat, MatDisplaySettings.TOP_LEFT);

            if(targetMat.width() > 0)
            {
                if(contourMat != null)
                    MatDisplay.DisplayMat(drawMat, MatDisplaySettings.BOTTOM_RIGHT);        
            }
            } else {




                if (warpedDrawMat.width() > 0)
                {
                    Mat blendMat = new Mat();
                    Core.addWeighted(camImageMat, 0.95f, warpedDrawMat, 0.5f, 0.0, blendMat);
                    MatDisplay.DisplayMat(blendMat, MatDisplaySettings.FULL_BACKGROUND);

                } else
                {
                    MatDisplay.DisplayMat(camImageMat, MatDisplaySettings.FULL_BACKGROUND);
                }
            }

        }


    }
}
