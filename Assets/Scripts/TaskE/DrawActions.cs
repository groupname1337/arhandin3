﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class DrawActions : MonoBehaviour, IVirtualButtonEventHandler {

    public GameObject drawButton;

    void Start()
    {
        drawButton.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        print("DRAW IS ON!");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        print("DRAW IS OFF!");
    }
}
