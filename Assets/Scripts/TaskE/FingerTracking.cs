﻿using UnityEngine;
using Vuforia;
using OpenCVForUnity;


public class FingerTracking : MonoBehaviour {

    Mat cameraImageMat;
    Mat cameraImageMatBlur = new Mat();
	
	void Update () {
        MatDisplay.SetCameraFoV(41.5f);

        Image cameraImage = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.RGBA8888);

        if(cameraImage!=null)
        {
            if(cameraImageMat==null)
            {
                cameraImageMat = new Mat(cameraImage.Height, cameraImage.Width, CvType.CV_8UC4);
            }
            cameraImageMat.put(0,0, cameraImage.Pixels);

            MatDisplay.DisplayMat(cameraImageMat, MatDisplaySettings.BOTTOM_LEFT);
            Imgproc.blur(cameraImageMat, cameraImageMatBlur, new Size(16,16));
            MatDisplay.DisplayMat(cameraImageMatBlur, MatDisplaySettings.FULL_BACKGROUND);
        }
	}
}
