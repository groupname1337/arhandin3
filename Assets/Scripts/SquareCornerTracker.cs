﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCVForUnity;
using Vuforia;

public class SquareCornerTracker : MonoBehaviour {

    private Mat camImageMat;
    private Camera cam;

    public double thres = 0.0;
    public double maxVal = 255.0;

    public float alpha = 1f, beta = 0.4f, gamma = 0f;
    public float aproxEpsilon = 10f;

    public bool showDebug = false;

    private MatOfPoint2f cornerPoints = new MatOfPoint2f();
    private MatOfPoint2f destinationPoints = new MatOfPoint2f();

    public GameObject renderTarget;

    Texture2D renderTex; // = new Texture2D(1024, 1024);

    // Use this for initialization
    void Start () {
        cam = Camera.main;
        cornerPoints.alloc(4);
        destinationPoints.alloc(4);
	}
	
	// Update is called once per frame
	void Update () {
        Image camImg = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.RGBA8888);
        if (camImg != null)
        {
            camImageMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
            camImageMat.put(0, 0, camImg.Pixels);
      

        Mat bw = new Mat();

        Imgproc.cvtColor(camImageMat, bw, Imgproc.COLOR_RGBA2GRAY);
        Mat threshold = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
        Imgproc.threshold(bw, threshold, thres, maxVal, Imgproc.THRESH_BINARY);

        // find countours using opencv
        Mat contours = new Mat();
        List<MatOfPoint> points = new List<MatOfPoint>();
        Imgproc.findContours(threshold, points, contours, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        Scalar color = new Scalar(255, 0, 0);
        Mat contourMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
        Imgproc.drawContours(contourMat, points, -1, color, 5);


        List<MatOfPoint2f> aprox = new List<MatOfPoint2f>();
            // Polygonize

        Mat polygonMat = new Mat(camImg.Height, camImg.Width, CvType.CV_8UC4);
        foreach (MatOfPoint point in points)
        {
                MatOfPoint2f p = new MatOfPoint2f();         
                MatOfPoint2f ap = new MatOfPoint2f();

                point.convertTo(p, CvType.CV_32F);
                Imgproc.approxPolyDP(p, ap, aproxEpsilon, true);
                
                aprox.Add(ap);
        }



            //MatDisplay.DisplayMat(threshold, MatDisplaySettings.FULL_BACKGROUND);
            // Debug mat
            Mat debugMat = new Mat();
            Imgproc.cvtColor(threshold, debugMat, Imgproc.COLOR_GRAY2RGBA);

            // run through approxmated points and draw debug circles
            float squareSize = 0f;
            foreach(MatOfPoint2f p in aprox)
            {
                // we are looking for a curve with only 4 points
                if(p.rows() == 4)
                {
                    Vector2[] corners = new Vector2[4];
                    for (int i = 0; i < p.rows(); i++)
                    {
                        Vector2 point = MatDisplay.GetPoint2f(p, i);
                        corners[i] = point;
                    }

                    // check distance between points
                    float side1 = pointDistance(corners[0], corners[1]);
                    float side2 = pointDistance(corners[1], corners[2]);
                    float side3 = pointDistance(corners[2], corners[3]);
                    float side4 = pointDistance(corners[3], corners[0]);
                    print("Length of square: " + (side1 + side2 + side3 + side4));
                    float size = side1 + side2 + side3 + side4;

                    if(size > squareSize)
                    {
                        squareSize = size;
                    }

                    // dot product of points to determine clockwise/counterclockwise
                    float dot1 = Vector2.Dot(corners[0], corners[1]);
                    float dot2 = Vector2.Dot(corners[1], corners[2]);
                    float dot3 = Vector2.Dot(corners[2], corners[3]);
                    float dot4 = Vector2.Dot(corners[3], corners[0]);

                    if(squareSize == size)
                    {
                        // draw lines between each point
                        Imgproc.line(debugMat, new Point(corners[0].x, corners[0].y), new Point(corners[1].x, corners[1].y), color, 3);
                        Imgproc.line(debugMat, new Point(corners[1].x, corners[1].y), new Point(corners[2].x, corners[2].y), color, 3);
                        Imgproc.line(debugMat, new Point(corners[2].x, corners[2].y), new Point(corners[3].x, corners[3].y), color, 3);
                        Imgproc.line(debugMat, new Point(corners[3].x, corners[3].y), new Point(corners[0].x, corners[0].y), color, 3);

                        Imgproc.circle(debugMat, new Point(corners[0].x, corners[0].y), 3, color, 5);
                        Imgproc.circle(debugMat, new Point(corners[1].x, corners[1].y), 3, color, 5);
                        Imgproc.circle(debugMat, new Point(corners[2].x, corners[2].y), 3, color, 5);
                        Imgproc.circle(debugMat, new Point(corners[3].x, corners[3].y), 3, color, 5);

                        // save our cornerpoints
                        cornerPoints.put(1, 0, corners[0].x, corners[0].y);
                        cornerPoints.put(0, 0, corners[1].x, corners[1].y);
                        cornerPoints.put(2, 0, corners[2].x, corners[2].y);
                        cornerPoints.put(3, 0, corners[3].x, corners[3].y);

                    }

                }                      
            }


            // okay, so we have our four corners
            // create destination points
            destinationPoints.put(0, 0, 0, 0);
            destinationPoints.put(1, 0, 1024, 0);
            destinationPoints.put(2, 0, 0, 1024);
            destinationPoints.put(3, 0, 1024, 1024);

            // homography - we just use the built in stuff this time
            Mat homography = Calib3d.findHomography(cornerPoints, destinationPoints);

            // create a texture from our image and homography
            Size matSize = new Size(1024, 1024); // texture size, and also matching our destination points
            Mat texMat = new Mat();
            Imgproc.warpPerspective(camImageMat, texMat, homography, matSize);
            Texture2D renderTex = new Texture2D(1024, 1024);
            MatDisplay.MatToTexture(texMat, ref renderTex);
            renderTarget.GetComponent<Renderer>().material.mainTexture = renderTex;

            // map the original object texture to the square

            Mat squareTex = new Mat();
            Mat texSrc = MatDisplay.LoadRGBATexture("Models/flying_skull_tex.png");
            Size texSize = new Size(640, 480);
            Imgproc.warpPerspective(texSrc, squareTex, homography.inv(), texSize);

            //MatDisplay.DisplayMat(debugMat, MatDisplaySettings.FULL_BACKGROUND);

            Mat blendMat = new Mat();
            Core.addWeighted(camImageMat, 0.95f, squareTex, 0.4f, 0.0, blendMat);

            if (showDebug)
            {
                MatDisplay.DisplayMat(debugMat, MatDisplaySettings.FULL_BACKGROUND);
            } else
            {
                MatDisplay.DisplayMat(blendMat, MatDisplaySettings.FULL_BACKGROUND);
            }

        }
    }

    float pointDistance(Vector2 p1, Vector2 p2)
    {
        return Mathf.Sqrt(Mathf.Pow(p2.x - p1.x, 2) + Mathf.Pow(p2.y - p1.y, 2));
    }
}