﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTextFromTarget : MonoBehaviour {

    public GameObject TextPickerObject;
    public GameObject TextPicker;
    public GameObject TextToChange;

    private TextMesh textResult;
    private TextMesh textSource;

    private List<string> stringList;
    private string stringResult = "";

    private Vector3 thisUpVector;
    private Vector3 thatUpVector;
    
    private Vector3 distanceVector;
    private float distanceValue;


    public float textChangeThreshold = 1.5f;
    private bool isWriting;
    public float waitTime = 1f;

    private IEnumerator WritingTimeout()
    {
        //print("FIRING");
        yield return new WaitForSeconds(waitTime);
        isWriting = false;
        //print("DONE");
    }

    // Use this for initialization
    void Start () {
        textResult = TextToChange.GetComponent<TextMesh>();
        textSource = TextPicker.GetComponent<TextMesh>();

        stringList = new List<string>();

    }

    // Update is called once per frame
    void Update () {
         
        if (!isWriting) //A timer fires every second to allow this to run.
        {

            isWriting = true;
            StartCoroutine(WritingTimeout()); //restart timer

            //calculatatify the angle between the upvectors:
            thisUpVector = transform.up.normalized;
            thatUpVector = TextPickerObject.transform.up.normalized;
            float updot = Vector3.Dot(thisUpVector, thatUpVector);

            //calc distance between (center of) objects
            distanceVector = transform.position - TextPickerObject.transform.position;
            distanceValue = distanceVector.magnitude;

            List<string> newLetters = new List<string>();

            if (0.5f > updot && updot > 0f) //if the objects are tipped relative to each other
            {
                print("updot: " + updot); //debug
                if (stringResult.Length>0) stringResult = stringResult.Remove(stringResult.Length - 1); //remove the last char in the string
            }

            else if (distanceValue <= textChangeThreshold)
            {
                newLetters.Add(textSource.text.ToString()); 
                //print(stringList); //debug

                foreach (string msg in newLetters)
                {
                    if (msg.Equals("_")) //WHERE DO ASTRONAUTS GO TO HANG OUT???
                    {
                        stringResult = stringResult + " "; //SPACEBAR!
                    }
                    else
                    {
                        stringResult = stringResult + msg;
                    }
                }

            }

        }

        textResult.text = stringResult;


    }
}
